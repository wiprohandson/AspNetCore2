﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Diagnostics.Contracts;

namespace ASP.NetCore_L2_HandsOn.Models
{
	public class ProductViewModel
	{
		public int ProductCategoryId { get; set; }
		public int ProductSubCategoryId { get; set; }
		public int ProductId { get; set; }

		public IEnumerable<SelectListItem> ProductCategories { get; set; } = new List<SelectListItem>();
        public IEnumerable<SelectListItem> ProductSubCategories { get; set; } = new List<SelectListItem>();
        public IEnumerable<SelectListItem> Products { get; set; } = new List<SelectListItem>();
    }
}
