﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASP.NetCore_L2_HandsOn.Models
{
	[Table("ProductSubCategories")]
	public class ProductSubCategory
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
		public int ProductCategoryId { get; set; }

		public IEnumerable<Product> Products { get; set; } = new List<Product>();
    }
}
