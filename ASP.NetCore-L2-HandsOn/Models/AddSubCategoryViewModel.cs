﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace ASP.NetCore_L2_HandsOn.Models
{
	public class AddSubCategoryViewModel
    {
		public string CategoryId { get; set; } = "";
		public IEnumerable<SelectListItem> Categories { get; set; } = new List<SelectListItem>(); 
	}
}
