﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace ASP.NetCore_L2_HandsOn.Models
{
	public class AddProductViewModel
    {
		public string SubCategoryId { get; set; } = "";
		public IEnumerable<SelectListItem> SubCategories { get; set; } = new List<SelectListItem>(); 
	}
}
