﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASP.NetCore_L2_HandsOn.Models
{
	[Table("Products")]
	public class Product
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public int ProductSubCategoryId { get; set; }
    }
}
