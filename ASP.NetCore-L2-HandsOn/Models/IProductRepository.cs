﻿namespace ASP.NetCore_L2_HandsOn.Models
{
    public interface IProductRepository
    {
        Task<IEnumerable<ProductCategory>> GetProductListAsync();
    }
}
