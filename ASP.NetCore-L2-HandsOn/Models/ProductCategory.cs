﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASP.NetCore_L2_HandsOn.Models
{
	[Table("ProductCategories")]
	public class ProductCategory
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;

        public IEnumerable<ProductSubCategory> ProductSubCategories { get; set; } = new List<ProductSubCategory>();

    }
}
