﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Identity.Client;

namespace ASP.NetCore_L2_HandsOn.Models
{
    public class SetUserAdminViewModel
    {
        public string SelectedUser { get; set; } = string.Empty;
        public List<SelectListItem> Users { get; set; } = new List<SelectListItem>();
        public bool MakeAdmin { get; set; } = false;
        public string whatever { get; set; } = "";
    }
}
