﻿using ASP.NetCore_L2_HandsOn.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace ASP.NetCore_L2_HandsOn.Controllers
{
    public class AccountController : Controller
    {
        public readonly UserManager<IdentityUser> UserManager;
        public readonly SignInManager<IdentityUser> SignInManager;
        public readonly RoleManager<IdentityRole> RoleManager;

        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
        }



        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new IdentityUser() { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("index", "home");
                } 
                
                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    return RedirectToAction("index", "home");
                }
                    
                ModelState.AddModelError("", "Wrong username or password.");
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await SignInManager.SignOutAsync();
            return RedirectToAction("index", "home");
        }

        [HttpGet]
        public async Task<IActionResult> SetUserAdmin()
        {
            var vm = new SetUserAdminViewModel()
            {
                Users = await UserManager.Users
                .Where(u => u.Email != null)
                .Select(u => new SelectListItem()
                {
                    Text = u.Email!,
                    Value = u.Email!
                })
                .ToListAsync()
            };

            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> SetUserAdmin(SetUserAdminViewModel model)
        {
            const string adminRoleName = "Admin";

            if (!(await RoleManager.RoleExistsAsync(adminRoleName)))
            {
                var adminRole = new IdentityRole
                {
                    Name = adminRoleName
                };
                await RoleManager.CreateAsync(adminRole);
            }

            var user = await UserManager.FindByEmailAsync(model.SelectedUser);
            if (user == null)
            {
                return RedirectToAction("index", "home");
            }

            if (model.MakeAdmin)
            {
                await UserManager.AddToRoleAsync(user, adminRoleName);
            }
            else
            {
                await UserManager.RemoveFromRoleAsync(user, adminRoleName);
            }

            return RedirectToAction("index", "home");
        }
    }
}
