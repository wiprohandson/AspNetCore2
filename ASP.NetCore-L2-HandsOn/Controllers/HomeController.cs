using ASP.NetCore_L2_HandsOn.Data;
using ASP.NetCore_L2_HandsOn.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace ASP.NetCore_L2_HandsOn.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ProductDbContext _productDbContext;
        private readonly UserManager<IdentityUser> _userManager;

        public HomeController(ILogger<HomeController> logger, ProductDbContext context, UserManager<IdentityUser> userMan)
        {
            _logger = logger;
            _productDbContext = context;
            _userManager = userMan;
        }

        public async Task<IActionResult> Index()
        {
            try
            {
                var vm = new ProductViewModel();
                vm.ProductCategories = await _productDbContext.Categories
                    .Select(c => new SelectListItem()
                    {
                        Value = c.Id.ToString(),
                        Text = c.Name
                    }).ToListAsync();

                return View(vm);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "error in Index view");
				ViewBag.ErrorTitle = "Error getting index";
				ViewBag.ErrorMessage = ex.ToString();
				return View("Error");
			}
        }

        public async Task<List<ProductSubCategory>> GetSubCategories(int id)
        {
            try
            {
                if (id == 0)
                    return [];

                return await _productDbContext.SubCategories.Where(s => s.ProductCategoryId == id).ToListAsync();
            }
            catch (Exception ex)
            {
				_logger.LogError(ex, "Error getting subcategories");
				throw;
            }
        }

        public async Task<List<Product>> GetProducts(int id)
        {

            try
            {
                if (id == 0)
                    return [];

                return await _productDbContext.Products.Where(p => p.ProductSubCategoryId == id).ToListAsync();
            }
            catch (Exception ex )
            {
                _logger.LogError(ex, "Error getting products");
                throw;
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult ErrorTest()
        {
            ViewBag.ErrorTitle = "TestError";
            ViewBag.ErrorMessage = "TestErrorMessage";
            return View("Error");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddCategory()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddCategory(string categoryName)
        {
            try
            {
                var newCategory = new ProductCategory() { Name = categoryName };
                _productDbContext.Categories.Add(newCategory);
                await _productDbContext.SaveChangesAsync();

                return Redirect("Index");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error adding category");
                ViewBag.ErrorTitle = "Error getting category";
                ViewBag.ErrorMessage = ex.ToString();
                return View("Error");
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddSubCategory()
        {
            try
            {
                var vm = new AddSubCategoryViewModel();
                vm.Categories = await _productDbContext.Categories
                    .Select(c => new SelectListItem()
                    {
                        Value = c.Id.ToString(),
                        Text = c.Name
                    }).ToListAsync();

                return View(vm);
            }
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error in subcategory view");
				ViewBag.ErrorTitle = "Error in subcategory view";
				ViewBag.ErrorMessage = ex.ToString();
				return View("Error");
			}
		}

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddSubCategory(string subCategoryName, int categoryId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(subCategoryName) || categoryId == 0)
                {
                    throw new ArgumentException("missing subcategoryName or categoryId");
                }
                var newSubCategory = new ProductSubCategory() { Name = subCategoryName, ProductCategoryId = categoryId };
                _productDbContext.SubCategories.Add(newSubCategory);
                await _productDbContext.SaveChangesAsync();

                return Redirect("Index");
            }
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error adding subcategory");
				ViewBag.ErrorTitle = "Error getting subcategory";
				ViewBag.ErrorMessage = ex.ToString();
				return View("Error");
			}

		}

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddProduct()
        {
            try
            {
                var vm = new AddProductViewModel();
                vm.SubCategories = await _productDbContext.SubCategories
                    .Select(c => new SelectListItem()
                    {
                        Value = c.Id.ToString(),
                        Text = c.Name
                    }).ToListAsync();

                return View(vm);
            }
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error getting product view");
				ViewBag.ErrorTitle = "Error getting product view";
				ViewBag.ErrorMessage = ex.ToString();
				return View("Error");
			}
		}

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddProduct(string productName, int subCategoryId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(productName) || subCategoryId == 0)
                {
                    throw new ArgumentException("missing productName or subCategoryId");
                }
                var newProduct = new Product() { Name = productName, ProductSubCategoryId = subCategoryId };
                _productDbContext.Products.Add(newProduct);
                await _productDbContext.SaveChangesAsync();
                return Redirect("Index");
            }
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error adding product");
				ViewBag.ErrorTitle = "Error adding product";
				ViewBag.ErrorMessage = ex.ToString();
				return View("Error");
			}

		}
    }
}
