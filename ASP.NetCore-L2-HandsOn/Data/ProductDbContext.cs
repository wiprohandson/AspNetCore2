﻿using ASP.NetCore_L2_HandsOn.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ASP.NetCore_L2_HandsOn.Data
{
    public class ProductDbContext : IdentityDbContext
	{
		public ProductDbContext(DbContextOptions<ProductDbContext> options)
			: base(options)
		{
		}

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> Categories { get; set; }
        public DbSet<ProductSubCategory> SubCategories { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ProductCategory>().HasData(
                new ProductCategory()
                {
                    Id = 1,
                    Name = "Category1"
                }
                );
            builder.Entity<ProductSubCategory>().HasData(
                new ProductSubCategory()
                {
                    Id = 1,
                    Name = "SubCategory1",
                    ProductCategoryId = 1
                }
                );

            builder.Entity<Product>().HasData(
                new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    ProductSubCategoryId = 1
                }
                );
        }
    }
}
